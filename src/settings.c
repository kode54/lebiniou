/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gstdio.h>
#include "settings.h"
#include "utils.h"
#include "constants.h"
#include "main.h"
#include "biniou.h"


extern char *data_dir;
static json_t *settings = NULL;
static gchar *json_settings = NULL;
uint8_t json_settings_type = CFG_DEFAULT;
extern double volume_scale;
extern enum startModes start_mode;
const char *start_modes[SM_NB] = { "last_created", "first_created", "last_updated" };
#ifdef WITH_WEBCAM
extern uint8_t hflip, vflip;
#endif
extern uint8_t usage_statistics;
static pthread_mutex_t settings_mutex;


static enum startModes
str2startModes(const char *str)
{
  if (is_equal(str, start_modes[SM_LAST])) {
    return SM_LAST;
  } else if (is_equal(str, start_modes[SM_FIRST])) {
    return SM_FIRST;
  } else if (is_equal(str, start_modes[SM_LAST_UPDATED])) {
    return SM_LAST_UPDATED;
  } else {
    fprintf(stderr, "[!] %s: Invalid startMode '%s'\n", __FILE__, str);
    return SM_LAST;
  }
}


void
Settings_new()
{
  settings = json_object();
  gchar *settings_dir = g_strdup_printf("%s/.%s", g_get_home_dir(), PACKAGE_NAME);
  mkdir(settings_dir, DIRECTORY_MODE);
  g_free(settings_dir);
  json_settings = g_strdup_printf("%s/.%s/%s", g_get_home_dir(), PACKAGE_NAME, JSON_SETTINGS);
  json_object_set_new(settings, "themes", json_pack("[sss]", "biniou", "covid-19", "zebulon"));
  xpthread_mutex_init(&settings_mutex, NULL);
}


void
Settings_delete()
{
  json_decref(settings);
  g_free(json_settings);
  xpthread_mutex_destroy(&settings_mutex);
}


json_t *
Settings_get()
{
  // DEBUG_JSON("settings", settings);
  return json_deep_copy(settings);
}


json_t *
Settings_get_input()
{
  return json_object_get(settings, "input");
}


json_t *
Settings_get_plugins()
{
  return json_copy(json_object_get(settings, "plugins"));
}


void
Settings_set_plugins(json_t *plugins)
{
  json_object_set(settings, "plugins", plugins);
}


static json_t *
param(json_t *o)
{
  return json_object_get(o, "name");
}


static json_t *
value(json_t *o)
{
  return json_object_get(o, "value");
}


static json_t *
update_plugins(const json_t *post, json_t *plugins, uint8_t *restart)
{
  size_t index1, index2;;
  json_t *obj;
  uint16_t old_enabled = 0, new_enabled = 0;

  // Disable all plugins
  json_array_foreach(plugins, index1, obj) {
    if (json_boolean_value(json_object_get(obj, "enabled"))) {
      old_enabled++;
    }
    json_object_set(obj, "enabled", json_false());
  }

  // Look for enabled plugins
  json_array_foreach(post, index1, obj) {
    if (is_equal(json_string_value(param(obj)), "plugins")) {
      json_t *plugin;
      // Update array
      json_array_foreach(plugins, index2, plugin) {
        if (is_equal(json_string_value(json_object_get(plugin, "name")), json_string_value(value(obj)))) {
          json_object_set(plugin, "enabled", json_true());
          new_enabled++;
        }
      }
    }
  }

  if (old_enabled != new_enabled) {
    *restart = 1;
  }

  return plugins;
}


static json_t *
get_themes(gchar *themes_dir, const uint8_t required)
{
  json_t *themes = json_array();
  GDir *dir = g_dir_open(themes_dir, 0, NULL);

  if (NULL != dir) {
    const gchar *entry = g_dir_read_name(dir);

    while (NULL != entry) {
      gchar *entry_dir = g_strdup_printf("%s/%s", themes_dir, entry);
      GStatBuf sbuf;

      if (!g_stat(entry_dir, &sbuf)) {
        if ((sbuf.st_mode & S_IFMT) == S_IFDIR) {
          json_array_append_new(themes, json_string(entry));
        }
      }
      g_free(entry_dir);
      entry = g_dir_read_name(dir);
    }
    g_dir_close(dir);
  } else {
    if (required) {
      xerror("%s: failed to read images directory %s\n", __FILE__, themes_dir);
    }
  }
  g_free(themes_dir);

  return themes;
}


static json_t *
get_base_themes()
{
  gchar *themes_dir = g_strdup_printf("%s/images", data_dir);

  return get_themes(themes_dir, 1);
}


static json_t *
get_user_themes()
{
  gchar *themes_dir = g_strdup_printf("%s/." PACKAGE_NAME "/images", g_get_home_dir());

  return get_themes(themes_dir, 0);
}


json_t *
Settings_get_themes()
{
  return json_deep_copy(json_object_get(settings, "themes"));
}


json_t *
Settings_get_all_themes()
{
  json_t *themes_j = json_object();

  json_object_set_new(themes_j, "baseThemes", get_base_themes());
  json_object_set_new(themes_j, "selectedThemes", Settings_get_themes());
  json_object_set_new(themes_j, "userThemes", get_user_themes());

  return themes_j;
}


static void
Settings_save()
{
  json_dump_file(settings, json_settings, JSON_INDENT(4));
  // DEBUG_JSON("settings", settings);
}


static void
Settings_plugins(uint16_t *enabled, uint16_t *disabled)
{
  size_t index;
  json_t *plugin;
  json_t *plugins = Settings_get_plugins();

  json_array_foreach(plugins, index, plugin) {
    if (json_boolean_value(json_object_get(plugin, "enabled"))) {
      ++*enabled;
    } else {
      ++*disabled;
    }
  }
  json_decref(plugins);
}


uint8_t
Settings_parse_post(Context_t *ctx, json_t *post)
{
  uint8_t restart = 0;
  json_t *old_plugins = Settings_get_plugins();
  json_t *old_themes = Settings_get_themes();
#ifdef WITH_WEBCAM
  int old_webcams = ctx->webcams;
#endif
  json_decref(settings);
  settings = json_object();
  size_t index;
  json_t *obj;
  const char *name = NULL; // input name
  json_t *input = json_object();
#ifndef FIXED
  json_t *screen = NULL;
#endif
  json_t *themes = json_array();
  json_t *engine = json_object();
  int sequencesMin = DELAY_MIN, sequencesMax = DELAY_MAX;
  int colormapsMin = DELAY_MIN, colormapsMax = DELAY_MAX;
  int imagesMin = DELAY_MIN, imagesMax = DELAY_MAX;
  uint8_t createSymlink = 0;
  uint8_t new_usage_statistics = 0;

#ifdef WITH_WEBCAM
  int webcamsMin = DELAY_MIN, webcamsMax = DELAY_MAX, webcams = 1;
  hflip = vflip = 0;
  json_object_set_new(engine, "hFlip", json_false());
  json_object_set_new(engine, "vFlip", json_false());
#endif
  json_object_set_new(settings, "statistics", json_false());

  // DEBUG_JSON("post", post);
  json_array_foreach(post, index, obj) {
    if (is_equal(json_string_value(param(obj)), "version")) {
      if (xatol(json_string_value(value(obj))) != SETTINGS_VERSION) {
        json_decref(old_plugins);
        return 0;
      }
    } else if (is_equal(json_string_value(param(obj)), "name")) {
      json_object_set(input, "name", value(obj));
      name = json_string_value(value(obj));
      if (!is_equal(name, input_plugin)) {
        restart = 1;
      }
    } else if (is_equal(json_string_value(param(obj)), "jackaudioLeft")) {
      assert(NULL != name);
      if (is_equal(name, "jackaudio")) {
        assert(NULL != input);
        json_object_set(input, "jackaudioLeft", value(obj));
      }
    } else if (is_equal(json_string_value(param(obj)), "jackaudioRight")) {
      assert(NULL != name);
      if (is_equal(name, "jackaudio")) {
        assert(NULL != input);
        json_object_set(input, "jackaudioRight", value(obj));
      }
    } else if (is_equal(json_string_value(param(obj)), "volumeScale")) {
      double volumeScale = xatof(json_string_value(value(obj)));
      json_object_set_new(input, "volumeScale", json_real(volumeScale));
      Context_set_volume_scale(ctx, volumeScale);
    } else if (is_equal(json_string_value(param(obj)), "width")) {
#ifndef FIXED
      assert(NULL == screen);
      screen = json_object();
      int new_width = xatol(json_string_value(value(obj)));
      if (new_width != width) {
        restart = 1;
      }
      json_object_set_new(screen, "width", json_integer(new_width));
#endif
    } else if (is_equal(json_string_value(param(obj)), "height")) {
#ifndef FIXED
      assert(NULL != screen);
      int new_height = xatol(json_string_value(value(obj)));
      if (new_height != height) {
        restart = 1;
      }
      json_object_set_new(screen, "height", json_integer(new_height));
#endif
    } else if (is_equal(json_string_value(param(obj)), "plugins")) {
      // handled by update_plugins() below
    } else if (is_equal(json_string_value(param(obj)), "themes")) {
      json_array_append(themes, value(obj));
    } else if (is_equal(json_string_value(param(obj)), "startMode")) {
      json_object_set(engine, "startMode", value(obj));
      start_mode = str2startModes(json_string_value(value(obj)));
#ifdef WITH_WEBCAM
    } else if (is_equal(json_string_value(param(obj)), "hFlip")) {
      json_object_set(engine, "hFlip", json_true());
      hflip = 1;
    } else if (is_equal(json_string_value(param(obj)), "vFlip")) {
      json_object_set(engine, "vFlip", json_true());
      vflip = 1;
    } else if (is_equal(json_string_value(param(obj)), "autoWebcamsMode")) {
      json_object_set(engine, "autoWebcamsMode", value(obj));
    } else if (is_equal(json_string_value(param(obj)), "webcamsMin")) {
      webcamsMin = xatol(json_string_value(value(obj)));
      json_object_set_new(engine, "webcamsMin", json_integer(webcamsMin));
    } else if (is_equal(json_string_value(param(obj)), "webcamsMax")) {
      webcamsMax = xatol(json_string_value(value(obj)));
      json_object_set_new(engine, "webcamsMax", json_integer(webcamsMax));
    } else if (is_equal(json_string_value(param(obj)), "webcams")) {
      int new_webcams = xatol(json_string_value(value(obj)));
      if (new_webcams != old_webcams) {
        restart = 1;
      }
      json_object_set_new(engine, "webcams", json_integer(new_webcams));
#else
    } else if (is_equal(json_string_value(param(obj)), "hFlip")) {
    } else if (is_equal(json_string_value(param(obj)), "vFlip")) {
    } else if (is_equal(json_string_value(param(obj)), "autoWebcamsMode")) {
    } else if (is_equal(json_string_value(param(obj)), "webcamsMin")) {
    } else if (is_equal(json_string_value(param(obj)), "webcamsMax")) {
    } else if (is_equal(json_string_value(param(obj)), "webcams")) {
    } else if (is_equal(json_string_value(param(obj)), "webcams")) {
#endif
    } else if (is_equal(json_string_value(param(obj)), "autoSequencesMode")) {
      json_object_set(engine, "autoSequencesMode", value(obj));
    } else if (is_equal(json_string_value(param(obj)), "sequencesMin")) {
      sequencesMin = xatol(json_string_value(value(obj)));
      json_object_set_new(engine, "sequencesMin", json_integer(sequencesMin));
    } else if (is_equal(json_string_value(param(obj)), "sequencesMax")) {
      sequencesMax = xatol(json_string_value(value(obj)));
      json_object_set_new(engine, "sequencesMax", json_integer(sequencesMax));
    } else if (is_equal(json_string_value(param(obj)), "autoColormapsMode")) {
      json_object_set(engine, "autoColormapsMode", value(obj));
    } else if (is_equal(json_string_value(param(obj)), "colormapsMin")) {
      colormapsMin = xatol(json_string_value(value(obj)));
      json_object_set_new(engine, "colormapsMin", json_integer(colormapsMin));
    } else if (is_equal(json_string_value(param(obj)), "colormapsMax")) {
      colormapsMax = xatol(json_string_value(value(obj)));
      json_object_set_new(engine, "colormapsMax", json_integer(colormapsMax));
    } else if (is_equal(json_string_value(param(obj)), "autoImagesMode")) {
      json_object_set(engine, "autoImagesMode", value(obj));
    } else if (is_equal(json_string_value(param(obj)), "imagesMin")) {
      imagesMin = xatol(json_string_value(value(obj)));
      json_object_set_new(engine, "imagesMin", json_integer(imagesMin));
    } else if (is_equal(json_string_value(param(obj)), "imagesMax")) {
      imagesMax = xatol(json_string_value(value(obj)));
      json_object_set_new(engine, "imagesMax", json_integer(imagesMax));
    } else if (is_equal(json_string_value(param(obj)), "randomMode")) {
      enum RandomMode randomMode = xatol(json_string_value(value(obj)));
      ctx->random_mode = randomMode;
      json_object_set_new(engine, "randomMode", json_integer(randomMode));
    } else if (is_equal(json_string_value(param(obj)), "maxFps")) {
      uint8_t maxFps = xatol(json_string_value(value(obj)));
      ctx->max_fps = maxFps;
      json_object_set_new(engine, "maxFps", json_integer(maxFps));
    } else if (is_equal(json_string_value(param(obj)), "fadeDelay")) {
      uint8_t fadeDelay = xatol(json_string_value(value(obj)));
      fade_delay = fadeDelay;
      json_object_set_new(engine, "fadeDelay", json_integer(fadeDelay));
    } else if (is_equal(json_string_value(param(obj)), "statistics")) {
      json_object_set_new(settings, "statistics", json_true());
      new_usage_statistics = usage_statistics = 1;
    } else if (is_equal(json_string_value(param(obj)), "flatpak")) {
      json_object_set_new(engine, "flatpak", is_equal(json_string_value(value(obj)), "false") ? json_false() : json_true());
    } else if (is_equal(json_string_value(param(obj)), "createSymlink")) {
      createSymlink = 1;
    } else {
      xerror("Unhandled POST parameter %s\n", json_string_value(param(obj)));
    }
  }
  biniou_set_delay(BD_SEQUENCES, sequencesMin, sequencesMax);
  Alarm_update(ctx->a_random, sequencesMin, sequencesMax);
  biniou_set_delay(BD_COLORMAPS, colormapsMin, colormapsMax);
  Alarm_update(ctx->a_cmaps, colormapsMin, colormapsMax);
  biniou_set_delay(BD_IMAGES, imagesMin, imagesMax);
  if (NULL != ctx->a_images) {
    Alarm_update(ctx->a_images, imagesMin, imagesMax);
  }
  sequences->shuffler->mode = Shuffler_parse_mode(json_string_value(json_object_get(engine, "autoSequencesMode")));
  ctx->cf->shf->mode = Shuffler_parse_mode(json_string_value(json_object_get(engine, "autoColormapsMode")));
  if (NULL != ctx->imgf) {
    ctx->imgf->shf->mode = Shuffler_parse_mode(json_string_value(json_object_get(engine, "autoImagesMode")));
  }
#ifdef WITH_WEBCAM
  biniou_set_delay(BD_WEBCAMS, webcamsMin, webcamsMax);
  if (webcams > 1) {
    ctx->webcams_shuffler->mode = Shuffler_parse_mode(json_string_value(json_object_get(engine, "autoWebcamsMode")));
    Alarm_update(ctx->a_webcams, webcamsMin, webcamsMax);
  }
#endif
  Context_websocket_send_state(ctx);

  if (!json_equal(themes, old_themes)) {
    restart = 1;
  }
  json_decref(old_themes);

  if (createSymlink) {
    int8_t ret = create_dirs();

    if (!ret) {
      printf("[i] Created symbolic link and directories\n");
    } else {
      fprintf(stderr, ("[!] Failed to create symbolic link and directories\n"));
    }
  }

  json_object_set_new(settings, "version", json_string(LEBINIOU_VERSION));
#ifndef FIXED
  json_object_set_new(settings, "screen", screen);
#endif
  json_object_set_new(settings, "input", input);
  json_object_set_new(settings, "themes", themes);
  json_object_set_new(settings, "engine", engine);
  json_object_set_new(settings, "plugins", update_plugins(post, old_plugins, &restart));
  json_t *stats = json_object_get(settings, "statistics");
  json_object_del(settings, "statistics");
  json_object_set_new(settings, "statistics", stats);

  // DEBUG_JSON("new settings", settings);
  Settings_save();

  if (usage_statistics) {
    uint16_t enabled = 0, disabled = 0;
    Settings_plugins(&enabled, &disabled);
    json_t *fields = json_pack("{sisisisisisi}",
                               "themes", json_array_size(themes),
                               "maxFps", ctx->max_fps,
                               "randomMode", ctx->random_mode,
                               "webcams", ctx->webcams,
                               "enabled", enabled,
                               "disabled", disabled);
    DEBUG_JSON("fields", fields);

    gchar *resolution = g_strdup_printf("%dx%d", WIDTH, HEIGHT);
    json_t *tags = json_pack("{sbsssssssb}",
                             "flatpak",
#ifdef FLATPAK
                             1,
#else
                             0,
#endif
                             "resolution", resolution,
                             "input", name,
                             "version", LEBINIOU_VERSION,
                             "optIn", new_usage_statistics);
    g_free(resolution);
    DEBUG_JSON("tags", tags);

    bulfius_post_report("/settings", fields, tags);
    json_decref(fields);
    json_decref(tags);
  }

  return restart;
}


const char *
Settings_get_configuration_file()
{
  return json_settings;
}


void
Settings_set_configuration_file(const char *file)
{
  g_free(json_settings);
  json_settings = g_strdup(file);
  json_settings_type = CFG_CUSTOM;
}


static uint16_t
get_integer(const json_t *o, const char *k)
{
  return json_integer_value(json_object_get(o, k));
}


#ifndef FIXED
static void
set_screen(const json_t *screen)
{
  width = get_integer(screen, "width");
  height = get_integer(screen, "height");
}
#endif


static void
set_input(const json_t *input)
{
  const char *name = json_string_value(json_object_get(input, "name"));
  const double volumeScale = json_real_value(json_object_get(input, "volumeScale"));

  if (NULL != name) {
    xfree(input_plugin);
    input_plugin = strdup(name);
  }
  if (volumeScale > 0) {
    volume_scale = volumeScale;
  }
}


static void
set_engine(Context_t *ctx, const json_t *engine)
{
  max_fps = get_integer(engine, "maxFps");
  fade_delay = get_integer(engine, "fadeDelay");
  start_mode = str2startModes(json_string_value(json_object_get(engine, "startMode")));
  random_mode = get_integer(engine, "randomMode");
  biniou_set_delay(BD_SEQUENCES, get_integer(engine, "sequencesMin"), get_integer(engine, "sequencesMax"));
  biniou_set_delay(BD_COLORMAPS, get_integer(engine, "colormapsMin"), get_integer(engine, "colormapsMax"));
  biniou_set_delay(BD_IMAGES, get_integer(engine, "imagesMin"), get_integer(engine, "imagesMax"));
#ifdef WITH_WEBCAM
  hflip = json_boolean_value(json_object_get(engine, "hFlip"));
  vflip = json_boolean_value(json_object_get(engine, "vFlip"));
  desired_webcams = get_integer(engine, "webcams");
#endif
}


void
Settings_finish(Context_t *ctx)
{
  json_t *engine = json_object_get(settings, "engine");
  if (NULL != engine) {
    sequences->shuffler->mode = Shuffler_parse_mode(json_string_value(json_object_get(engine, "autoSequencesMode")));
    ctx->cf->shf->mode = Shuffler_parse_mode(json_string_value(json_object_get(engine, "autoColormapsMode")));
    if (NULL != ctx->imgf) {
      ctx->imgf->shf->mode = Shuffler_parse_mode(json_string_value(json_object_get(engine, "autoImagesMode")));
    }
  } else {
    sequences->shuffler->mode = ctx->cf->shf->mode = BS_SHUFFLE;
    if (NULL != ctx->imgf) {
      ctx->imgf->shf->mode = BS_SHUFFLE;
    }
  }
#ifdef WITH_WEBCAM
  if (ctx->webcams > 1) {
    if (NULL != engine) {
      biniou_set_delay(BD_WEBCAMS, get_integer(engine, "webcamsMin"), get_integer(engine, "webcamsMax"));
      ctx->webcams_shuffler->mode = Shuffler_parse_mode(json_string_value(json_object_get(engine, "autoWebcamsMode")));
    } else {
      biniou_set_delay(BD_WEBCAMS, DELAY_MIN, DELAY_MAX);
      ctx->webcams_shuffler->mode = BS_CYCLE;
    }
  }
#endif
}


void
Settings_load(Context_t *ctx)
{
  json_t *loaded_settings = json_load_file(json_settings, 0, NULL);

  if (NULL != loaded_settings) {
    json_decref(settings);
    settings = loaded_settings;
    json_t *statistics = json_object_get(settings, "statistics");
    if (NULL != statistics) {
      usage_statistics = json_boolean_value(statistics);
    }
#ifndef FIXED
    set_screen(json_object_get(settings, "screen"));
#endif
    set_input(json_object_get(settings, "input"));
    set_engine(ctx, json_object_get(settings, "engine"));
  }
}


void
Settings_lock()
{
  xpthread_mutex_lock(&settings_mutex);
}


void
Settings_unlock()
{
  xpthread_mutex_unlock(&settings_mutex);
}


uint8_t
Settings_is_favorite(const char *name)
{
  size_t index;
  json_t *value;
  uint8_t ret = 0;
  json_t *plugins = json_object_get(settings, "plugins");

  json_array_foreach(plugins, index, value) {
    if (is_equal(json_string_value(json_object_get(value, "name")), name)) {
      json_t *fav = json_object_get(value, "favorite");

      if (json_is_boolean(fav)) {
        ret = json_boolean_value(fav);
      } else { // no favorite status
        ret = 0;
      }

      return ret;
    }
  }

  return ret;
}


uint8_t
Settings_switch_favorite(const char *name)
{
  size_t index;
  json_t *value;
  uint8_t ret = 0;

  Settings_lock();
  json_t *plugins = json_object_get(settings, "plugins");

  json_array_foreach(plugins, index, value) {
    if (is_equal(json_string_value(json_object_get(value, "name")), name)) {
      json_t *fav = json_object_get(value, "favorite");

      if (json_is_boolean(fav)) { // switch value
        uint8_t is_favorite = json_boolean_value(fav);
        ret = !is_favorite;
        json_object_set(value, "favorite", json_boolean(ret));
      } else { // no favorite status, set to true
        json_object_set_new(value, "favorite", json_true());
        ret = 1;
      }
    }
  }
  Settings_save();
  Settings_unlock();

  return ret;
}
