if OS_DARWIN
lebiniou_PROGRAMS = liblebiniou.dylib
else
lebiniou_PROGRAMS = liblebiniou.so.0
endif
lebinioudir = $(libdir)
liblebiniou_so_0_CFLAGS = -fPIC ${MAGICKWAND_CFLAGS}
liblebiniou_so_0_SOURCES = alarm.c alarm.h \
	brandom.c brandom.h \
	btimer.c btimer.h \
	buffer_8bits.c buffer_8bits.h \
	buffer_RGBA.c buffer_RGBA.h \
	bulfius_command2str.c bulfius_post.c \
	cmap_8bits.c cmap_8bits.h \
	cmapfader.c cmapfader.h	\
	colormaps.c colormaps.h \
	context.c context_banks.c context_get_input_size.c \
	cmapfader_command.c \
	commands.c \
	context_export.c context.h \
	context_commands.c context_json.c \
	context_set.c context_shortcuts.c \
	delay.c \
	fader.c fader.h \
	image_8bits_misc.c \
	imagefader.c imagefader.h \
	imagefader_command.c \
	image_filter.c image_filter.h \
	images_misc.c \
	input.c input.h \
	globals.c globals.h \
	layer.c layer.h \
	params3d.c params3d.h \
	particles.c particles.h \
	paths.c paths.h \
	plugin.c plugin.h plugin_parameters.c \
	plugins_command.c \
	plugins.c plugins.h \
	oscillo.c oscillo.h \
	schemes_random.c \
	sequence.c sequence.h \
	sequence2json.c \
	sequence_copy.c sequences_find.c \
	sequence_load.c sequence_save.c \
	sequencemanager.c sequencemanager.h \
	sequencemanager_command.c \
	shuffler.c shuffler.h shuffler_modes.h \
	settings.c settings.h \
	spline.c spline.h \
	translation.c translation.h \
	ui_commands_banks.c \
	utils.c utils.h \
	bulfius_allowed_commands.c


if EXTRA_OPENGL
liblebiniou_so_0_SOURCES += context_gl.c
endif

if EXTRA_WEBCAM
liblebiniou_so_0_SOURCES += webcam_controls.c webcam_start_stop.c webcam_loop.c \
	webcam_open_close.c webcam_init_uninit.c webcam_options.c webcam.h
endif

liblebiniou_so_0_LDFLAGS = -shared -Wl,-soname,liblebiniou.so.0
liblebiniou_so_0_LDADD = ${SWSCALE_LIBS} ${FFTW3_LIBS} ${ULFIUS_LIBS} ${ORCANIA_LIBS}

if OS_DARWIN
liblebiniou_dylib_CFLAGS = ${liblebiniou_so_0_CFLAGS}
liblebiniou_dylib_SOURCES = ${liblebiniou_so_0_SOURCES}
liblebiniou_dylib_LDFLAGS = -shared
liblebiniou_dylib_LDADD = ${liblebiniou_so_0_LDADD}
endif

bin_PROGRAMS = lebiniou
lebiniou_SOURCES = main.c commands_key.h cmdline.c signals.c main.h
lebiniou_SOURCES += \
	bulfius.h bulfius.c \
	bulfius_get_colormap.c \
	bulfius_get_image.c \
	bulfius_get_frame.c \
	bulfius_get_parameters.c \
	bulfius_get_plugins.c \
	bulfius_get_sequence.c \
	bulfius_get_statistics.c \
	bulfius_post_sequence.c \
	bulfius_post_command.c \
	bulfius_post_parameters.c \
	bulfius_post_plugins.c \
	bulfius_ui_get_data.c \
	bulfius_ui_get_settings.c bulfius_ui_post_settings.c \
	bulfius_ui_get_static.c \
	bulfius_ui_post_command.c \
	bulfius_websockets.c \
	context_settings.c \
	context_new_delete.c \
	context_playlist.c context_png.c \
	context_ui_commands.c \
	image_8bits.c image_8bits.h \
	image_filter.h \
	images.c images.h \
	options.c options.h \
	screenshot.c \
	biniou.c biniou.h \
	circle.c circle.h \
	commands.h \
	constants.h \
	context_run.c \
	context_statistics.c \
	includes.h \
	keys.h \
	point2d.h point3d.h \
	rgba.h \
	schemes.c schemes.h schemes_str2option.c \
	sequences.c sequences.h \
	ui_commands.h

BUILT_SOURCES = commands.h commands.c
CLEANFILES = commands.h commands.c
dist_lebiniou_SOURCES = commands.c.in gen.awk man.awk defaults.h.in COMMANDS.md.awk \
	commands.h.head commands.h.tail commands_enum.awk
nodist_lebiniou_SOURCES = commands.h commands.c

BUILT_SOURCES += bulfius_get_commands.c bulfius_str2command.c bulfius_command2str.c
CLEANFILES += bulfius_get_commands.c bulfius_str2command.c bulfius_command2str.c
dist_lebiniou_SOURCES += bulfius_get_commands.awk bulfius_str2command.awk bulfius_command2str.awk
nodist_lebiniou_SOURCES += bulfius_get_commands.c bulfius_str2command.c bulfius_command2str.c

if OS_DARWIN
lebiniou_DEPENDENCIES = liblebiniou.dylib
else
lebiniou_DEPENDENCIES = liblebiniou.so.0
endif
lebiniou_CFLAGS = -fPIE -fPIC ${MAGICKWAND_CFLAGS}
lebiniou_LDFLAGS = -pthread
if OS_DARWIN
lebiniou_LDADD = -L. -llebiniou ${MAGICKWAND_LIBS} ${ULFIUS_LIBS} ${ORCANIA_LIBS}
else
lebiniou_LDADD = -L. -l:liblebiniou.so.0 ${MAGICKWAND_LIBS} ${ULFIUS_LIBS} ${ORCANIA_LIBS}
endif

commands.h: commands.h.head commands.c.in commands.h.tail commands_enum.awk
	@echo "Generating "$@
	@cp -f commands.h.head $@
	@$(AWK) -f $(DESTDIR)$(srcdir)/commands_enum.awk $(DESTDIR)$(srcdir)/commands.c.in >> $@
	@cat commands.h.tail >> $@

commands.c: commands.c.in gen.awk commands.h
	@echo "Generating "$@
	@$(AWK) -f $(DESTDIR)$(srcdir)/gen.awk $(DESTDIR)$(srcdir)/commands.c.in > $@

bulfius_get_commands.c: commands.c.in bulfius_get_commands.awk
	@echo "Generating "$@
	@$(AWK) -f $(DESTDIR)$(srcdir)/bulfius_get_commands.awk $(DESTDIR)$(srcdir)/commands.c.in > $@

bulfius_str2command.c: commands.c.in bulfius_str2command.awk
	@echo "Generating "$@
	@$(AWK) -f $(DESTDIR)$(srcdir)/bulfius_str2command.awk $(DESTDIR)$(srcdir)/commands.c.in > $@

bulfius_command2str.c: commands.c.in bulfius_command2str.awk
	@echo "Generating "$@
	@$(AWK) -f $(DESTDIR)$(srcdir)/bulfius_command2str.awk $(DESTDIR)$(srcdir)/commands.c.in > $@
