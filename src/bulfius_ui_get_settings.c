/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "settings.h"


int
callback_ui_get_settings(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  const Context_t *ctx = (const Context_t *)user_data;
  assert(NULL != ctx);
  json_t *settings = Context_settings(ctx);

  ulfius_set_json_body_response(response, 200, settings);
  json_decref(settings);

  return U_CALLBACK_COMPLETE;
}
