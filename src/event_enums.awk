BEGIN {
  print "enum "ENVIRON["ENUM"]" {";
  N = 0;
}


{
  if (N == 0)
    print "    "$1" = 0,";
  else
    print "    "$1",";

  N = N + 1;
}


END {
  print "};";
  print "";
}